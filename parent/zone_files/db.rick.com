;
;
; BIND data file for rick.com.
;
$TTL	604800
@		IN	SOA	rick.com. root.rick.com. (
		           4449		; Serial
			 604800		; Refresh
			  86400		; Retry
			2419200		; Expire
			 604800 )	; Negative Cache TTL
;
@		IN	NS	ns.rick.com.
@		IN	A	172.2.0.1
@		IN	AAAA	::1
ns		IN	A	172.2.0.12
www		IN	A	172.2.0.1
mail		IN	A	172.2.0.11
@		IN	MX	20	mail.rick.com.
mailqueue	IN	A	172.2.0.85
@		IN	MX	30	mailqueue.rick.com:
;
