Note this instruction is not valid at the moment.
# project for learning dns on debian based systems, e.g. Ubuntu, Mint

* create a heirarchy
* subdomains
* different records: A (address) MX (Mail Exchanger) NS (Name Server) PTR (Pointers)
* Queries with nslookup, dig, host, resolvers aka ping ssh nc

## references (URL)
* https://help.ubuntu.com/lts/serverguide/dns-configuration.html
* https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-a-private-network-dns-server-on-ubuntu-14-04

## System Prep
Make sure the system is updated, install `bind9` and 'git' (not needed to for editing zone files).
```
sudo apt-get update
sudo apt-get install bind9 git
```
## Cloning Gitlab Repo
Clone the repo and move to the new directory.
```
git clone https://gitlab.com/rpwimer/dns-learning.git
cd dns-learning
```
Or as one full command
```
git clone https://gitlab.com/rpwimer/dns-learning.git && cd dns-learning
```

## Deploying the files in the repo
* Copy the files below to /etc/bind
```
sudo cp named_conf/named.conf.local /etc/bind/
sudo cp zone_files/db.rick.com /etc/bind/
sudo cp zone_files/db.hats.net /etc/bind/
```

* Check the status of bind9 and restart it.
```
service bind9 status
sudo service bind9 restart
```
If you want to check the syslog to make sure that there were no errors, run the following in another terminal:
```
tail -f /var/log/syslog
```

## Using nslookup to verify that you are getting the correct ip address. The answer should be 172.2.0.10 for "www.rick.com"
```
nslookup www.rick.com localhost
```
The answer should look like this

```
Server:		localhost
Address:	127.0.0.1#53

Name:	www.rick.com
Address: 172.2.0.10
```
## Intent of Zone Files
Computers communicate via ip addresses, but as humans, names are much easier to remember than numbers. So zone file is a text dababase of computers with their names and ip address.   

## MX Records
* Each domain should have 2 mailservers (MX records) in case one of them goes down. Note that the mail servers have a number before the name of the server, this denotes priority of mail with the lower numbers having a higher priority. 
* There will be an MX record and an A record for each mailserver.
```
mail            IN      A       172.2.0.11
@               IN      MX      20      mail.rick.com.
mailqueue       IN      A       172.2.0.85
@               IN      MX      30      mailqueue.rick.com.
``` 


